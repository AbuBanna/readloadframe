program LoadFrameReadWrite	!************************************************************************************************
!
! The purpose of this code is to (a) read the output data (*.plt) from a 'Load Frame', which has three features (time, defomration and force)
!                                (b) calculate engineering stress, strain with cross-section dimentions as input parameters
!                                (c) write a new file with stress, strain distribution over time
!
!----------------------------------------------------------------------------------------------------------------------------
!
! Intel Fortran
!
! Md Abu Horaira Banna, University of Alabama, mbanna@crimson.ua.edu, Version 01, 2017
!
!----------------------------------------------------------------------------------------------------------------------------
		implicit none
		
		real*8			:: stress0, strain0, stiffness, a, maxstress, w, h, area, l0
		real*8			:: b0, stressy, strainy, b1, pointy, defy 
		integer*4		:: n, iostatus, i, ii, j, step1, step2	
		real*8, allocatable			:: time(:), def(:), force(:), stress(:), strain(:)		
		n = 0
		l0 = 4.68d+00					! inital length of test specimen
		w = 0.51180d+00					! inital width of test specimen
		h = 0.12598d+00					! initial height of test specimen
		open(unit = 100, file = 'a.plt', status = 'old', action = 'read')
		do 
				read( 100,*, iostat = iostatus ) 
				if ( iostatus .LT. 0 ) exit 
				if ( iostatus .GT. 0 ) then
						print *, 'error'
						exit
				end if
				n = n + 1				! counting number of lines in the input data file
		end do
		close(100)
		
		allocate ( time(0:n-1) )
		allocate ( def(0:n-1) )
		allocate ( force(0:n-1) )
		allocate ( stress(0:n-1) )
		allocate ( strain(0:n-1) )
		
		area = w * h					! cross-section area of the test section
		open(unit = 101, file = 'a.plt', status = 'old', action = 'read')
		open(unit = 100, file = 'output.plt')
		do i = 0, n-1
				read( 101, * ) time(i), def(i), force(i)
				stress(i) = force (i) / area		! stress
				strain (i) = def(i) / l0			! strain
				write( 100, '(3e18.10)' ) time(i), stress(i), strain(i)		! writing stress strain distribution oever time
				if ( def (i) .LE. 0.04 ) step1 = i 							! first reference point to calculate elsatic modulus
				if ( def (i) .LE. 0.06 ) step2 = i 							! Second reference point to calculate elsatic modulus
		end do
		close(101)
		close(100)
		
		stress0 = stress(step2) - stress(step1)		
		strain0 = strain(step2) - strain(step1)
		a = stress0 / strain0	! calculation of stiffness/ elastic modulus
		b1 = l0 * 2 / 100
		do j = ( step2 + 100 ), n-1
				stressy = stress(j) - stress(step2)
				pointy = b1 + strain(step2) + ( stressy / a )
				if ( strain(j) .GE. pointy ) then
						stressy = stress(j)
						strainy = strain(j)
						defy = def(j)
						exit
				end if
		end do
				
		maxstress = 0
		! finding the strenght (mas stress) of the specimen
		do i = 0, n-1
			if 	( abs( stress( i ) ) .GT. abs( maxstress ) ) then
					maxstress = stress( i )
					ii = i
			end if
		end do		
		open ( unit = 10, file='data.plt')
		write (10, '(5e18.10)' ) abs(a), abs(maxstress), strain(ii), stressy, defy, strainy 
		close (10)
		
		deallocate ( time )
		deallocate ( def )
		deallocate ( force )
		deallocate( stress )
		deallocate ( strain )
		
end program
